#!/bin/bash

function query() {
    SOLR_HOST=$(echo "$PLATFORM_RELATIONSHIPS" | base64 --decode | jq -r '.status_solr[0].host')
    SOLR_PORT=$(echo "$PLATFORM_RELATIONSHIPS" | base64 --decode | jq -r '.status_solr[0].port')

    if [ -z "${SOLR_HOST}" ]; then
        exit
    fi

    # memory-heap-max is default
    SOLR_METRICS_PREFIX="memory.heap.max"
    SOLR_METRICS_GROUP="jvm"
    if [ "$1" == "memory-heap-used" ]; then
        SOLR_METRICS_PREFIX="memory.heap.used"
    elif [ "$1" == "system-cpu-load" ]; then
        SOLR_METRICS_PREFIX="os.systemCpuLoad"
    elif [ "$1" == "index-size" ]; then
        SOLR_METRICS_GROUP="core"
        SOLR_METRICS_PREFIX="INDEX.sizeInBytes"
    fi

    URL="http://${SOLR_HOST}:${SOLR_PORT}/solr/admin/metrics?wt=json&group=${SOLR_METRICS_GROUP}&prefix=${SOLR_METRICS_PREFIX}"
    SOLR_METRIC_RESPONSE=$(curl $URL 2>/dev/null)
    METRIC_VALUE=$(echo ${SOLR_METRIC_RESPONSE} | json_pp |  grep -E "\"value\"|\"${SOLR_METRICS_PREFIX}\"" | grep -Eo '[0-9]+([.][0-9]+)?' | awk '{n += $1}; END{print n}')

    echo $METRIC_VALUE
}

if [ $# == 0 ]; then
    echo $"Usage $0 {memory-heap-max|memory-heap-used|system-cpu-load|index-size}"
    exit
else
    query "$1"
fi