# Platform.sh Zabbix Template

[Platform.sh](https://platform.sh/) provides basic capabilities to for the monitoring via [Health notifications](https://docs.platform.sh/integrations/notifications.html). But in some advanced cases, you might need to monitor some specific metrics of a specific app service. And [Zabbix](https://www.zabbix.com/) is a perfect tool for doing it.

This solution lets you install [Zabbix](https://www.zabbix.com/) on top of any existing Platform.sh PHP project.

Also, it provides a Platform.sh Zabbix template, which allows monitoring of the following services:

- Platform.sh common: database size, free storage, etc
- OPCache
- PHP-FPM
- Redis
- Solr
- Varnish

Usage examples:

![](docs/images/opcache-memory-used.png)

![](docs/images/redis-lookups.png)

![](docs/images/solr-cpu.png)

![](docs/images/varnish-requests.png)

## Installation

1. Add zabbix sources and utility scripts from this repo to your project sources:
    ```bash
    git clone git@gitlab.com:contextualcode/platformsh-zabbix-template.git ~/platformsh-zabbix-template
    cp -R ~/platformsh-zabbix-template/zabbix <YOUR-PROJECT-PATH>/
    ```

2. Add a `zabbix` database: 
    - `.platform/services.yaml`:
        ```yaml
        mysqldb:
            ...
            configuration:
                schemas:
                    ...
                    - zabbix
                endpoints:
                    ...
                    zabbix:
                        default_schema: zabbix
                        privileges:
                            zabbix: admin
        ```
    - `.platform.app.yaml`:
        ```yaml
        relationships:
            ...
            # Zabbix relationships
            database_zabbix: mysqldb:zabbix
        ```

3. Enable required PHP extensions in `.platform.app.yaml`:
    ```yaml
    runtime:
        extensions:
            ...
            - ctype
            - xmlreader
            - xmlwriter
            - gettext
    ```

4. Add the install script for [Platform.sh CLI](https://docs.platform.sh/development/cli.html) to `build` hook in `.platform.app.yaml`:
    ```yaml
    hooks:
        build: |
            ...
            curl -sS https://platform.sh/cli/installer | php
    ```

5. Add the zabbix install scripts to the `build` and `deploy` hooks in `.platform.app.yaml`:
    ```yaml
    hooks:
        build: |
            ...
            /app/zabbix/scripts/install-libevent.sh
            /app/zabbix/scripts/install-zabbix.sh
            /app/zabbix/scripts/install-composer-ui.sh
        deploy: |
            ...
            /app/zabbix/scripts/update-configs.sh
            /app/zabbix/scripts/install-db.sh
            /app/zabbix/scripts/start-zabbix.sh
    ```

6. Add a `zabbix/configs` mount in `.platform.app.yaml`:
    ```yaml
    mounts:
        ...
        zabbix/configs:
            source: local
            source_path: zabbix-configs
    ```

7. Update the PHP configuration in `.platform.app.yaml`:
    ```yaml
    variables:
        php:
            max_input_time: 300
    ```

8. Add a `zabbix` web location in `.platform.app.yaml` (please make sure it is the first one in the locations list):
    ```yaml
    web:
        locations:
            /zabbix:
                root: zabbix/src/zabbix/ui
                passthru: true
                scripts: true
                index: [index.php]
            ...
    ```

9. If Varnish is used on your project, please make sure all requests to the Zabbix UI bypass it and are served directly by PHP. Please update `.platform/routes.yaml` in order to achieve this:
    ```yaml
    https://{default}/zabbix/:
        type: upstream
        upstream: app:http
        cache:
            enabled: false
        ssi:
            enabled: false
    ```

10. Add PHP dependencies in `.platform.app.yaml`:
    ```yaml
    dependencies:
        php:
            ...
            wizaplace/php-fpm-status-cli: "^1.0"
            gordalina/cachetool: "^5.0"
    ```

11. For each Platform.sh service which needs to be monitored by Zabbix add `status_<service>` relationship in `.platform.app.yaml`. It should point to the existing service endpoints:
    ```yaml
    relationships:
        ...
        status_database: mysqldb:user
        status_solr: solrsearch:collection1
        status_redis: rediscache:redis
    ```

12. If Varnish is used on your project, and you want to collect its metrics in Zabbix:
    - Add `varnish-stats` app to your project:
        ```bash
        cp -R ~/platformsh-zabbix-template/varnish-stats <YOUR-PROJECT-PATH>/
        ```
    - Setup [Platform.sh API Token](https://docs.platform.sh/development/cli/api-tokens.html) on your project:
        ```bash
        platform variable:create \
            --project=<PROJECT-ID> \
            --level=project \
            --environment=master \
            --name="PLATFORMSH_CLI_TOKEN" \
            --value="<TOKEN-VALUE>" \
            --visible-build=false \
            --sensitive=true \
            --json=false \
            --prefix=env \
            --visible-runtime=true \
            --enabled=true \
            --inheritable=true
        ```

13. Deploy all the changes:
    ```bash
    git add zabbix .platform.app.yaml .platform/routes.yaml .platform/services.yaml varnish-stats/.platform.app.yaml
    git commit -m "Installed Zabbix"
    git push platform master
    ```

14. Once the changes are deployed you should be able to open [https://your-project-domain/zabbix/](https://project-domain/zabbix/) and login using default credentials:
    - Username: Admin
    - Password: zabbix

## Configuration

1. Change the default password:
    - From the left navigation open "Administration" -> "Users"
    - Edit Admin user by clicking "Admin" in the users list
    - Click "Change password" button
    - Provide a new password
    - Click "Update" button

2. Import the "Platform.sh by ContextualCode" Zabbix template:
    - From the left navigation open "Configuration" -> "Templates"
    - Click "Import" button (top right)
    - Use [`zabbix/templates/platformsh-by-contextualcode.yml`](https://gitlab.com/contextualcode/platformsh-zabbix-template/-/raw/master/zabbix/templates/platformsh-by-contextualcode.yml) as "Import file" 
    - Click "Import" button

3. Update Zabbix templates:
    - From the left navigation open "Configuration" -> "Hosts"
    - Click "Zabbix server" from the hosts list
    - Select "Templates" tab
    - Click "Unlink and clear" for "Linux by Zabbix agent" and "Zabbix Server" default templates
    - Type "Platform.sh by ContextualCode" in "Link new templates" field
    - Click "Update" button

## Usage

To check all the collected metrics open "Monitoring" -> "Latest data" from the left navigation. And you will see:

![](docs/images/latest-data.png)

And for each of the metrics you can access the historical data by clicking on "Graph":

![](docs/images/history-solr-ram.png)

## Notifications and alerts

Besides allowing for monitoring Zabbix provides capabilities to send out notifications and alerts when a metric matches specific conditions.

Some additional configuration is required, in order to use Zabbix alerts and notifications:

1. Setup delivery channels. In terms of Zabbix they are called [Media Types](https://www.zabbix.com/documentation/current/manual/config/notifications/media):
    - From the left navigation open "Administration" -> "Media types"
    - Setup "Email (HTML)" Media type following https://www.zabbix.com/documentation/current/manual/config/notifications/media/email.
    - Setup any other needed Media types.

2. Assign "Media types" to administrator user:
    - From the left navigation open "Administration" -> "Users"
    - Click "Admin" user
    - Open "Media" tab
    - Add new item with "Email (HTML)" type and provide the administrator email as value for "Send to" field.
    - Assign any other needed Media types.
    - Click "Update" button to save assigned Medias

3. Enable notifications:
    - From the left navigation open "Configuration" -> "Actions"
    - Click "Report problems to Zabbix administrators"
    - Enable "Enabled" checkbox
    - Click "Update"

Now media types are set, and the notifications are enabled. And we are ready to setup the first notification.

Let's set up a notification for the case when there is less than 500MB of free persistent storage space:

1. From the left navigation open "Configuration" -> "Hosts"
2. Click "Zabbix server" from the hosts list
3. Open "Triggers" tab
4. Click "Create trigger" button (top right)
5. Use "Low persistent storage space" as a name
6. Select "High" severity
7. Click "Expression constructor" link
8. Click "Edit" button in expression constructor
9. Select "Platform.sh: Available storage" item
10. For the "Result" field use "<524288000" value
11. Click "Insert" button in expression constructor
12. Click "Add" button in expression constructor
13. Click "Add" button

    ![](docs/images/trigger-low-storage.png)

After the trigger is added, each time when there is less than 500MB of available storage the notification will be sent out to the Admin user. To test it out, you can ssh to your project and generate a big test file in one of your mounts:
```bash
platform ssh -A app
dd if=/dev/zero of=/app/zabbix/configs/output.dat bs=512M count=1
```
